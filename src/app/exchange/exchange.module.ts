import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { ExchangeRoutingModule } from './exchange-routing.module';
import { MaterialModule } from '../material/material.module';

import { ExchangeComponent } from './pages/exchange/exchange.component';


@NgModule({
  declarations: [
    ExchangeComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MaterialModule,
    ExchangeRoutingModule
  ],
  exports:[
  ]
})
export class ExchangeModule { }
