import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Currencies,ConvertFrom } from '../interfaces/currencies.interface';

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {

  constructor( private http: HttpClient ) {}

  getCurrencies(): Observable<Currencies> {
    const myheader = new HttpHeaders({'Authorization' : 'Basic ZnJlZWxhbmNlcjc4Nzg5ODE4MDptMmlzYmlmcWw3N3JuM3Z0dDFtdjNucjAxMA=='});
    return this.http.get<Currencies>('https://xecdapi.xe.com/v1/currencies.json', { headers: myheader });
    // return this.http.request<Currencies>('GET','https://xecdapi.xe.com/v1/currencies.json', { headers: myheader });

  };

  swap(c1:string, c2:string, v:number): Observable<ConvertFrom> {
    const myheader = new HttpHeaders({'Authorization' : 'Basic ZnJlZWxhbmNlcjc4Nzg5ODE4MDptMmlzYmlmcWw3N3JuM3Z0dDFtdjNucjAxMA=='});
    return this.http.get<ConvertFrom>(`https://xecdapi.xe.com/v1/convert_from.json/?from=${ c1 }&to=${ c2 }&amount=${ v }`, { headers: myheader });
  };
}
