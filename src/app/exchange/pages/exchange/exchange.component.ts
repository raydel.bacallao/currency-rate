import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConvertFrom, Currency, To } from '../../interfaces/currencies.interface';
import { ExchangeService } from '../../services/exchange.service';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.css']
})
export class ExchangeComponent implements OnInit {

  currencies: Currency[] = [];

  errorConexion: boolean = false;

  form: FormGroup = this.fb.group({
    moneda1: ['', Validators.required],
    moneda2: ['', Validators.required],
    monto: ['', [Validators.required, Validators.min(0.01)]],
    result: 0,
    rate: 0,
  })

  constructor( private exchangeService: ExchangeService,
               private fb: FormBuilder ) {
  }

  ngOnInit(): void {
    this.exchangeService.getCurrencies()
      .subscribe( currencies => {
        this.currencies = currencies.currencies;
      },
      (error) => this.errorConexion = true
      );
  }

  Swap(){
    let swap1 = this.form.value.moneda1;
    let swap2 = this.form.value.moneda2;
    this.form.patchValue({
      moneda1: swap2,
      moneda2: swap1
    });
    this.Convert();
  }

  Convert(){
    if ( this.form.valid ){
      this.exchangeService.swap(this.form.value.moneda1,this.form.value.moneda2,this.form.value.monto)
        .subscribe( convertFrom => {
          this.form.patchValue({
            result: convertFrom.to[0].mid,
            rate: convertFrom.to[0].mid/this.form.value.monto
          });
        },
        (error) => this.errorConexion = true);
    }
  }
}